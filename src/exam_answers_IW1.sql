-- Question 1
CREATE TABLE players (
    id SERIAL,
    username VARCHAR(128) NOT NULL,
    rank INTEGER DEFAULT 1000,
    PRIMARY KEY (id),
    CHECK (rank > 0)
);

-- OR

CREATE TABLE players (
    id SERIAL PRIMARY KEY,
    username VARCHAR(128) NOT NULL,
    rank INTEGER CHECK (rank > 0) DEFAULT 1000
);

-- Question 2
CREATE TABLE games (
    id SERIAL PRIMARY KEY,
    name VARCHAR(92) NOT NULL,
    editor_name VARCHAR(120) NOT NULL
);

-- Question 3
ALTER TABLE games DROP editor_name;
ALTER TABLE games ADD editor_id INTEGER REFERENCES editors(id);

-- OR

ALTER TABLE games RENAME editor_name TO editor_id;
ALTER TABLE games ALTER editor_id TYPE INTEGER USING editor_id::INTEGER;
ALTER TABLE games ADD CONSTRAINT games_editor_id_fkey FOREIGN KEY (editor_id) REFERENCES editors (id);

-- Question 4
SELECT * FROM games WHERE editor_id > 7;

-- Question 5
SELECT name FROM games ORDER BY name LIMIT 18 OFFSET 12;
